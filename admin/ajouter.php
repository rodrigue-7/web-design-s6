<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Administration | MANKAFY BOUTIQUE</title>
    <meta name="description" content="Espace d'administration du site de vente MANKAFY BOUTIQUE">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/flag-icon.min.css">
    <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="assets/scss/style.css">
    <link href="assets/css/lib/vector-map/jqvmap.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

</head>
<body>
    <?php include('left_panel.php') ?>
<!-- Right Panel -->
    <div id="right-panel" class="right-panel">
        <!-- Header-->
        <?php include('header.php') ?>
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Ajouter un produit</h1>
                    </div>
                </div>
            </div>
        </div>


        <div class="content mt-3">
            <?php if(isset($_GET['info'])){ ?>
            <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Success</span> Insertion r&eacute;ussi avec suc&egrave;e!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <?php }elseif (isset($_GET['info2'])) { ?>
                <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-danger">Erreur</span> Echec d'insertion!!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php } ?>
            <form method="POST" enctype="multipart/form-data" action="../fonction/ajouter_produit.php">
                <div class="col-sm-12">
                    <div class="card">
                          <div class="card-header"><strong>Details</strong><small> Produit</small></div>
                          <div class="card-body card-block">
                            <div class="form-group"><label for="company" class=" form-control-label">Nom</label><input type="text" name="nom" placeholder="Nom du produit" class="form-control"></div>
                            <div class="form-group"><label for="vat" class=" form-control-label">Description</label><input type="text" name="description" placeholder="Description" class="form-control"></div>
                            <div class="form-group"><label for="street" class=" form-control-label">Prix</label><input type="text" name="prix" placeholder="Prix en £" class="form-control"></div>
                            <div class="form-group"><label for="street" class=" form-control-label">Reference</label><input type="text" name="reference" placeholder="Reference" class="form-control"></div>
                            <div class="form-group"><label for="street" class=" form-control-label">Image</label><br><input type="file" name="image"></div>
                            <div class="form-group"><label for="street" class=" form-control-label">Title</label><input type="text" name="title" placeholder="Titre image" class="form-control"></div>
                          </div>
                    </div>
                </div>
                <div class="card-footer">
                            <button type="submit" name="sumit" class="btn btn-success btn-sm">
                              <i class="fa fa-dot-circle-o"></i> Ajouter
                            </button>
                            <button type="reset" class="btn btn-danger btn-sm">
                              <i class="fa fa-ban"></i> Reset
                            </button>
                </div>
            </form>
        </div>
    </div>

    <script src="assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/lib/vector-map/jquery.vmap.js"></script>
    <script src="assets/js/lib/vector-map/jquery.vmap.min.js"></script>
    <script src="assets/js/lib/vector-map/jquery.vmap.sampledata.js"></script>
    <script src="assets/js/lib/vector-map/country/jquery.vmap.world.js"></script>
    <script>
        ( function ( $ ) {
            "use strict";

            jQuery( '#vmap' ).vectorMap( {
                map: 'world_en',
                backgroundColor: null,
                color: '#ffffff',
                hoverOpacity: 0.7,
                selectedColor: '#1de9b6',
                enableZoom: true,
                showTooltip: true,
                values: sample_data,
                scaleColors: [ '#1de9b6', '#03a9f5' ],
                normalizeFunction: 'polynomial'
            } );
        } )( jQuery );
    </script>

</body>
</html>

<?php
  	require 'fonction/fonction.php';
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
		<title>BOUTIQUE Nike | Vente en ligne d'acc&eacute;soire de FootBall</title>
		<link href="web/css/style.css" rel='stylesheet' type='text/css' />
		<meta name="Description" content="Vente et Collection des divers produits de FootBall de marque Nike" /> 
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		</script>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
		<script src="web/js/jquery.min.js"></script>
		<script type="text/javascript" src="web/js/jquery.easy-ticker.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			$('#demo').hide();
			$('.vticker').easyTicker();
		});
		</script>
		<link href="web/css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
		<script type="text/javascript" src="web/js/megamenu.js"></script>
		<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
		<script src="web/js/menu_jquery.js"></script>
		<link rel="stylesheet" href="web/css/slippry.css">
		<script src="web/js/jquery-ui.js" type="text/javascript"></script>
		<script src="web/js/scripts-f0e4e0c2.js" type="text/javascript"></script>
		<script>
			  jQuery('#jquery-demo').slippry({
			  slippryWrapper: '<div class="sy-box jquery-demo" />', 
			  adaptiveHeight: false, 
			  useCSS: false, 
			  autoHover: false,
			  transition: 'fade'
			});
		</script>
		<!----start-pricerage-seletion---->
		<script type="text/javascript" src="web/js/jquery-ui.min.js"></script>
		<link rel="stylesheet" type="text/css" href="web/css/jquery-ui.css">
		<script type='text/javascript'>//<![CDATA[ 
			$(window).load(function(){
			 $( "#slider-range" ).slider({
			            range: true,
			            min: 0,
			            max: 500,
			            values: [ 100, 400 ],
			            slide: function( event, ui ) {  $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
			            }
			 });
			$( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) + " - $" + $( "#slider-range" ).slider( "values", 1 ) );
			
			});//]]>  
		</script>
		<!----//End-pricerage-seletion---->
		<!---move-top-top---->
		<script type="text/javascript" src="web/js/move-top.js"></script>
		<script type="text/javascript" src="web/js/easing.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
				});
			});
		</script>
		<!---//move-top-top---->
	</head>
	<body>
		<!---start-wrap---->
		<?php include('web/menu_top.php'); ?>
        <?php include('web/image_slide.php'); ?>
        <!--- start-content---->
		<div class="content">
			<div class="wrap">
				<div class="content-left">
						<div class="content-left-top-grid">
							<div class="content-left-price-selection">
								<h4>Sélectionnez un prix:</h4>
								<div class="price-selection-tree">
									<span class="col_checkbox">
										<input id="10" class="css-checkbox10" type="checkbox">
										<label class="normal"><i for="10" name="demo_lbl_10"  class="css-label10"> </i> 400</label>
									</span>
									<span class="col_checkbox">
										<input id="11" class="css-checkbox11" type="checkbox">
										<label class="active1"><i for="11" name="demo_lbl_11"  class="css-label11"> </i>350</label>
									</span>
									<span class="col_checkbox">
										<input id="12" class="css-checkbox12" type="checkbox">
										<label class="normal"><i for="12" name="demo_lbl_12"  class="css-label12"> </i> 300</label>
									</span>
									<span class="col_checkbox">
										<input id="13" class="css-checkbox13" type="checkbox">
										<label class="normal"><i for="13" name="demo_lbl_13"  class="css-label13"> </i>250</label>
									</span>
									<span class="col_checkbox">
										<input id="14" class="css-checkbox14" type="checkbox">
										<label class="normal"><i for="14" name="demo_lbl_14"  class="css-label14"> </i> 200</label>
									</span>
									<span class="col_checkbox">
										<input id="15" class="css-checkbox15" type="checkbox">
										<label class="normal"><i for="15" name="demo_lbl_15"  class="css-label15"> </i>150</label>
									</span>
								</div>	
							</div>
						</div>
						<div class="content-left-bottom-grid">
							<h4>Autres articles :</h4>
							<div class="content-left-bottom-grids">
                                <?php 
                                    $autres = selectAutres();
                                    foreach ($autres as $key) {
                                ?>
								<div class="content-left-bottom-grid1">
									<img src="data:image;base64,<?php echo $key['image'];?>" title="<?php echo $key['nom'];?>" />
									<h5><a href="#"><?php echo $key['nom'];?></a></h5>
									<span> <?php echo $key['categorie'];?></span>
									<label>&#163; <?php echo $key['prix'];?></label>
								</div>
								<?php } ?>
							</div>
						</div>
				</div>
				<d
				+iv class="content-right">
					<div class="product-grids">
						<!--- start-rate---->
							<script src="web/js/jstarbox.js"></script>
							<link rel="stylesheet" href="web/css/jstarbox.css" type="text/css" media="screen" charset="utf-8" />
							<script type="text/javascript">
								jQuery(function() {
									jQuery('.starbox').each(function() {
										var starbox = jQuery(this);
										starbox.starbox({
											average: starbox.attr('data-start-value'),
											changeable: starbox.hasClass('unchangeable') ? false : starbox.hasClass('clickonce') ? 'once' : true,
											ghosting: starbox.hasClass('ghosting'),
											autoUpdateAverage: starbox.hasClass('autoupdate'),
											buttons: starbox.hasClass('smooth') ? false : starbox.attr('data-button-count') || 5,
											stars: starbox.attr('data-star-count') || 5
										}).bind('starbox-value-changed', function(event, value) {
											if(starbox.hasClass('random')) {
												var val = Math.random();
												starbox.next().text(' '+val);
												return val;
											} 
										})
									});
								});
							</script>
						

						<?php 
						    $liste = selectProduit(); 
                            foreach ($liste as $key) {
						?>
						<div class="product-grid fade last-grid">
							<div class="product-grid-head">
								<ul class="grid-social">
									<li><a class="facebook" href="#"><span> </span></a></li>
									<li><a class="twitter" href="#"><span> </span></a></li>
									<li><a class="googlep" href="#"><span> </span></a></li>
									<div class="clear"> </div>
								</ul>
								<div class="block">
									<div class="starbox small ghosting"> </div> <span> (46)</span>
								</div>
							</div>

							<div class="product-pic">
								<a href="produit-<?php echo $key['reference']; ?>-<?php echo $key['id']; ?>-detail.htm"><?php echo '<img src="data:image;base64,'.$key['image'].'" title="'.$key['title'].'" alt="'.$key['title'].'">'; ?></a><p>
								<a href="produit-<?php echo $key['reference']; ?>-<?php echo $key['id']; ?>-detail.htm"><?php echo $key['nom']; ?></a>
								<span>Chaussure de football</span>
								</p>
							</div>
							<div class="product-info">
								<div class="product-info-cust">
									<a href="produit-<?php echo $key['reference']; ?>-<?php echo $key['id']; ?>-detail.htm">Details</a>
								</div>
								<div class="product-info-price">
									<a href="produit-<?php echo $key['reference']; ?>-<?php echo $key['id']; ?>-detail.htm">&#163; <?php echo $key['prix']; ?></a>
								</div>
								<div class="clear"> </div>
							</div>
							<div class="more-product-info">
								<span> </span>
							</div>
						</div>
                        <?php } ?>


						<div class="clear"> </div>
					</div>
				</div>
				<div class="clear"> </div>
			</div>
		</div>
		<?php include('web/footer.php'); ?>
	</body>
</html>


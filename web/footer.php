<div class="bottom-grids">
			<div class="bottom-top-grids">
				<div class="wrap">
					<div class="bottom-top-grid">
						<h4>OBTENIR DE L'AIDE</h4>
						<ul>
							<li><a href="contact.html">Contactez nous</a></li>
							<li><a href="">Achats</a></li>
							<li><a href="">NIKEiD</a></li>
							<li><a href="">Nike+</a></li>
						</ul>
					</div>
					<div class="bottom-top-grid">
						<h4>ORDRES</h4>
						<ul>
							<li><a href="">Options de paiement</a></li>
							<li><a href="">Exp&eacute;dition et livraison</a></li>
							<li><a href="">R&eacute;sultats</a></li>
						</ul>
					</div>
					<div class="bottom-top-grid last-bottom-top-grid">
						<h4>REGISTRE</h4>
						<p>Cr&eacute;ez un compte pour g&eacute;rer tout ce que vous faites avec Nike, de vos pr&eacute;f&eacute;rences d'achat &agrave; votre activit&eacute; Nike +.</p>
						<a class="learn-more" href="">Apprendre encore plus</a>
					</div>
					<div class="clear"> </div>
				</div>
			</div>
			<div class="bottom-bottom-grids">
				<div class="wrap">
					<div class="bottom-bottom-grid">
						<h6>INSCRIPTION PAR COURRIER ELECTRONIQUE</h6>
						<p>Soyez le premier &agrave; connaitre les nouveaux produits et les offres sp&eacute;ciales.</p>
						<a class="learn-more" href="">S'inscrire maintenant</a>
					</div>
					<div class="bottom-bottom-grid">
						<h6>CARTES CADEAUX</h6>
						<p>Donne le cadeau qui correspond toujours.</p>
						<a class="learn-more" href="">Afficher les cartes</a>
					</div>
					<div class="bottom-bottom-grid last-bottom-bottom-grid">
						<h6>MAGASINS PRES DE VOUS</h6>
						<p>Localisez un magasin de d&eacute;tail Nike ou un d&eacute;taillant autoris&eacute;.</p>
						<a class="learn-more" href="">Chercher</a>
					</div>
					<div class="clear"> </div>
				</div>
			</div>
		</div>
		
		<div class="footer">
			<div class="wrap">
				<div class="footer-left">
					<ul>
						<li><a href="">Conditions d'utilisation</a> <span> </span></li>
						<li><a href="">Nike Inc.</a> <span> </span></li>
						<li><a href="">Site par ANDRIANOMENJANAHARY Rodrigue N-07</a></li>
						<div class="clear"> </div>
					</ul>
				</div>
				<div class="footer-right">
					<script type="text/javascript">
						$(document).ready(function() {
							$().UItoTop({ easingType: 'easeOutQuart' });
						});
					</script>
			    <a href="" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>
				</div>
				<div class="clear"> </div>
			</div>
		</div>
		
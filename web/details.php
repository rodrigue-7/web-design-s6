<?php 
   require '../fonction/fonction.php';
   $id = $_GET['id'];
 ?>
<!DOCTYPE HTML>
<html>
<head>
		<?php   
		        $getTitle =  selectProduitDetail($id);
                foreach ($getTitle as $nom) { ?>
				<title>BOUTIQUE Nike | Chaussure de FootBall - <?php echo $nom['nom']; ?></title>
				<meta name="viewport" content="<?php echo $nom['description']; ?>">
		<?php } ?>
		<link href="web/css/style.css" rel='stylesheet' type='text/css' />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		</script>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
		<script src="web/js/jquery.min.js"></script>
		<script type="text/javascript" src="web/js/jquery.easy-ticker.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			$('#demo').hide();
			$('.vticker').easyTicker();
		});
		</script>	
		<link href="web/css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
		<script type="text/javascript" src="web/js/megamenu.js"></script>
		<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
		<script src="web/js/menu_jquery.js"></script>
		<script type="text/javascript" src="web/js/move-top.js"></script>
		<script type="text/javascript" src="web/js/easing.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
				});
			});
		</script>
		<link href="web/css/owl.carousel.css" rel="stylesheet">
		<script src="web/js/jquery-1.9.1.min.js"></script> 
		<script src="web/js/owl.carousel.js"></script>
		<script>
		    $(document).ready(function() {
		      $("#owl-demo").owlCarousel({
		        items : 3,
		        lazyLoad : true,
		        autoPlay : true,
		        navigation : true,
			    navigationText : ["",""],
			    rewindNav : false,
			    scrollPerPage : false,
			    pagination : false,
    			paginationNumbers: false,
		      });
		
		    });
		</script>   
</head>
	<body>
		
        <!---start-header---->
			<div class="header">
				<div class="top-header">
					<div class="wrap">
						<div class="top-header-left">
							<ul>
								<!---cart-tonggle-script---->
								<script type="text/javascript">
									$(function(){
									    var $cart = $('cart');
									        $('clickme').click(function(e) {
									         e.stopPropagation();
									       if ($cart.is(":hidden")) {
									           $cart.slideDown("slow");
									       } else {
									           $cart.slideUp("slow");
									       }
									    });
									    $(document.body).click(function () {
									       if ($cart.not(":hidden")) {
									           $cart.slideUp("slow");
									       } 
									    });
									    });
								</script>
								<!---//cart-tonggle-script---->
								<li><a class="cart" href=""><span id="clickme"> </span></a></li>
								<!---start-cart-bag---->
								<div id="cart">Votre panier <span>(0)</span></div>
								<!---start-cart-bag---->
								<li><a class="info" href=""><span> </span></a></li>
							</ul>
						</div>
						<div class="top-header-center">
							<div class="top-header-center-alert-left">
								<?php   
								        $getName =  selectProduitDetail($id);
						                foreach ($getName as $nom) { ?>
										<h1><?php echo $nom['nom']; ?></h1>
								<?php } ?>
								
							</div>
							<div class="top-header-center-alert-right">
								<div class="vticker">
								  	<ul>
									  	<li>Plusieurs couleurs et formes disponible</li>
								  	</ul>
								</div>
							</div>
							<div class="clear"> </div>
						</div>
						<div class="top-header-right">
							<ul>
								<li><a href="login.html">Admin</a></li>
							</ul>
						</div>
						<div class="clear"> </div>
					</div>
				</div>
				<br>
				<!----start-bottom-header---->
				<div class="header-bottom">
					<div class="wrap">
					<!-- start header menu -->
							<ul class="megamenu skyblue">
								<li class="grid"><a class="color2" href=""><h4>HOMME</h4></a></li>
					  			<li class="active grid"><a class="color4" href=""><h4>FEMME</h4></a></li>				
								<li><a class="color5" href=""><h4>ENFANTS</h4></a></li>
								<li><a class="color6" href=""><h4>SPORTS</h4></a></li>
								<li><a class="color7" href=""><h4>NIKE TENU DE SPORT</h4></a></li>
							</ul>
					</div>
				</div>
				</div>
				<!----//End-bottom-header---->

		<div class="content details-page">
			<div class="product-details">
				<div class="wrap">
					<ul class="product-head">
						<li><a href="index.php">Accueil</a> <span>::</span></li>
						<?php $getNom =  selectProduitDetail($id);
                              foreach ($getNom as $nom) { ?>
						<li class="active-page"><a href=""><h4><?php echo $nom['nom']; ?></h4></a></li>
						<?php } ?>
						<div class="clear"> </div>
					</ul>
					<link rel="stylesheet" href="web/css/etalage.css">
					<script src="web/js/jquery.etalage.min.js"></script>
				
				<script>
						jQuery(document).ready(function($){
							$('#etalage').etalage({
								thumb_image_width: 300,
								thumb_image_height: 400,
								source_image_width: 900,
								source_image_height: 1000,
								show_hint: true,
								click_callback: function(image_anchor, instance_id){
									alert('Callback example:\nYou clicked on an image with the anchor: "'+image_anchor+'"\n(in Etalage instance: "'+instance_id+'")');
								}
							});
							$('.dropdownlist').change(function(){
								etalage_show( $(this).find('option:selected').attr('class') );
							});
					});
				</script>

				<div class="details-left">
					<div class="details-left-slider">
						<ul id="etalage">
                            <?php 
                                $images = selectImageProduitDetail($id);
                                foreach ($images as $key) {
                            ?>
							<li>
								<img class="etalage_thumb_image" alt="<?php echo $key['nom']; ?>" title="<?php echo $key['nom']; ?>" src="data:image;base64,<?php echo $key['image']; ?>"/>
								<img class="etalage_source_image" alt="<?php echo $key['nom']; ?>" title="<?php echo $key['nom']; ?>" src="data:image;base64,<?php echo $key['image']; ?>" />
							</li>
							<?php } ?>
						</ul>
					</div>
					<?php 
					    $detail =  selectProduitDetail($id);
                        foreach ($detail as $key) {
					?>
					<div class="details-left-info">
						<div class="details-right-head">
							<h2><?php echo $key['nom']; ?></h2>
							<br>
							<ul class="pro-rate">
								<li><a class="product-rate" href="#"> <label> </label></a> <span> </span></li>
							</ul>
							<h3 class="learn-more">Details</h3>
							<p class="product-detail-info"><h6><?php echo $key['description']; ?></h6></p>
							<a class="learn-more" href="#"></a>
							<div class="product-more-details">
								<ul class="price-avl">
									<li class="price"><span>&#163; 153.39</span><label>&#163; <?php echo $key['prix']; ?></label></li>
									<li class="stock"><i>En Stock</i></li>
									<div class="clear"> </div>
								</ul>
								<br>
								<br>
								<ul class="product-colors">
									<h3>Couleur disponible ::</h3>
									<li><a class="color1" href="#"><span> </span></a></li>
									<li><a class="color2" href="#"><span> </span></a></li>
									<li><a class="color3" href="#"><span> </span></a></li>
									<li><a class="color4" href="#"><span> </span></a></li>
									<li><a class="color5" href="#"><span> </span></a></li>
									<li><a class="color6" href="#"><span> </span></a></li>
									<li><a class="color7" href="#"><span> </span></a></li>
									<li><a class="color8" href="#"><span> </span></a></li>
									<div class="clear"> </div>
								</ul>
							</div>
						</div>
					</div>
					<?php } ?>
					<div class="clear"> </div>
				</div>
				<div class="details-right"></div>
				<div class="clear"></div>
			</div>


			<div class="product-reviwes">
				<div class="wrap">

					<script src="js/easyResponsiveTabs.js" type="text/javascript"></script>
					<script type="text/javascript">
						$(document).ready(function () {
							 $('#horizontalTab').easyResponsiveTabs({
									type: 'default', //Types: default, vertical, accordion           
									width: 'auto', //auto or any width like 600px
									fit: true,   // 100% fit in a container
									closed: 'accordion', // Start closed if in accordion view
									activate: function(event) { // Callback function if tab is switched
									var $tab = $(this);
									var $info = $('#tabInfo');
									var $name = $('span', $info);
										$name.text($tab.text());
										$info.show();
									}
								});
													
							 $('#verticalTab').easyResponsiveTabs({
									type: 'vertical',
									width: 'auto',
									fit: true
								 });
						 });
					</script>

				<!--vertical Tabs-->
        		<div id="verticalTab">
		            <div class="resp-tabs-container vertical-tabs">
		                <div>
		                	<h3>Avis des clients</h3>
		                	<p>Il n'y a pas encore d'&eacute;valuation client.</p>
		                </div>
		            </div>
		        </div>
		       
       		<div class="clear"> </div>
       		
       		<div class="similar-products">
       			<div class="similar-products-left">
       				<h3>Chaussure NIKE</h3>
       				<p>Quelque produits semblable ou identique &agrave; l'article ci-dessus mais aves des formes et des couleurs differents</p>
       			</div>
       			<div class="similar-products-right">
							<script src="js/jstarbox.js"></script>
							<link rel="stylesheet" href="css/jstarbox.css" type="text/css" media="screen" charset="utf-8" />
							<script type="text/javascript">
								jQuery(function() {
									jQuery('.starbox').each(function() {
										var starbox = jQuery(this);
										starbox.starbox({
											average: starbox.attr('data-start-value'),
											changeable: starbox.hasClass('unchangeable') ? false : starbox.hasClass('clickonce') ? 'once' : true,
											ghosting: starbox.hasClass('ghosting'),
											autoUpdateAverage: starbox.hasClass('autoupdate'),
											buttons: starbox.hasClass('smooth') ? false : starbox.attr('data-button-count') || 5,
											stars: starbox.attr('data-star-count') || 5
										}).bind('starbox-value-changed', function(event, value) {
											if(starbox.hasClass('random')) {
												var val = Math.random();
												starbox.next().text(' '+val);
												return val;
											} 
										})
									});
								});
							</script>
					        <div id="owl-demo" class="owl-carousel">
				                <?php $liste = selectProduit();
                                    foreach ($liste as $key) {
					       	    ?>
				                <div class="item" onclick="location.href='details.html';">
				                	<div class="product-grid fade sproduct-grid">
										<div class="product-pic">
											<a href="produit-<?php echo $key['reference']; ?>-<?php echo $key['id']; ?>-detail.htm"><?php echo '<img src="data:image;base64,'.$key['image'].'" title="'.$key['title'].'" alt="'.$key['title'].'">'; ?></a>
											<p>
											<a href="produit-<?php echo $key['reference']; ?>-<?php echo $key['id']; ?>-detail.htm"><?php echo $key['nom']; ?></a>
											<span>Chaussure de football</span>
											</p>
										</div>
										<div class="product-info">
											<div class="product-info-cust">
												<a href="produit-<?php echo $key['reference']; ?>-<?php echo $key['id']; ?>-detail.htm">Details</a>
											</div>
											<div class="product-info-price">
												<a href="produit-<?php echo $key['reference']; ?>-<?php echo $key['id']; ?>-detail.htm">&#163; <?php echo $key['prix']; ?></a>
											</div>
											<div class="clear"> </div>
										</div>
										<div class="more-product-info">
											<span> </span>
										</div>
									</div>
				                </div>
				                <?php } ?>
			              </div>
       			    </div>
       			<div class="clear"> </div>
       		</div>
       		
		</div>
      </div>	
	<?php include('footer.php'); ?>
	</body>
</html>


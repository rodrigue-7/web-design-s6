<!DOCTYPE HTML>
<html>
	<head>
		<title>BOUTIQUE Nike | Connexion - Administration</title>
		<link href="web/css/style.css" rel='stylesheet' type='text/css' />
		<meta name="Description" content="Page de connexion pour les administrateurs">
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		</script>
		<!----webfonts---->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
		<!----//webfonts---->
		<script src="web/js/jquery.min.js"></script>
		<!----start-alert-scroller---->
		<script type="text/javascript" src="web/js/jquery.easy-ticker.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			$('#demo').hide();
			$('.vticker').easyTicker();
		});
		</script>
		<!----start-alert-scroller---->
		<!-- start menu -->
		<link href="web/css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
		<script type="text/javascript" src="web/js/megamenu.js"></script>
		<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
		<script src="web/js/menu_jquery.js"></script>
		<!-- //End menu -->
		<!---move-top-top---->
		<script type="text/javascript" src="web/js/move-top.js"></script>
		<script type="text/javascript" src="web/js/easing.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
				});
			});
		</script>
		<!---//move-top-top---->
	</head>
	<body>
		
		<!---start-header---->
			<div class="header">
				<div class="top-header">
					<div class="wrap">
						<div class="top-header-left">
							<ul>
								<!---cart-tonggle-script---->
								<script type="text/javascript">
									$(function(){
									    var $cart = $('cart');
									        $('clickme').click(function(e) {
									         e.stopPropagation();
									       if ($cart.is(":hidden")) {
									           $cart.slideDown("slow");
									       } else {
									           $cart.slideUp("slow");
									       }
									    });
									    $(document.body).click(function () {
									       if ($cart.not(":hidden")) {
									           $cart.slideUp("slow");
									       } 
									    });
									    });
								</script>
								<!---//cart-tonggle-script---->
								<li><a class="cart" href=""><span id="clickme"> </span></a></li>
								<!---start-cart-bag---->
								<div id="cart">Votre panier <span>(0)</span></div>
								<!---start-cart-bag---->
								<li><a class="info" href=""><span> </span></a></li>
							</ul>
						</div>
						<div class="top-header-center">
							<div class="top-header-center-alert-left">
								<h1>CONNEXION - Espace administration</h1>
							</div>
							<div class="top-header-center-alert-right">
								<div class="vticker">
								  	<ul>
									  	<li>BOUTIQUE Nike</li>
								  	</ul>
								</div>
							</div>
							<div class="clear"> </div>
						</div>
						<div class="top-header-right">
							<ul>
								<li><a href="index.html">Accueil</a></li>
							</ul>
						</div>
						<div class="clear"> </div>
					</div>
				</div>
				<br>
				<!----start-bottom-header---->
				<div class="header-bottom">
					<div class="wrap">
					
					</div>
				</div>
		</div>
		<!----//End-bottom-header---->
		
		<div class="content login-box">
			<div class="login-main">
				<div class="wrap">
					<br>
						<h2>ESPACE ADMINISTRATEUR</h2>
                    <br>
					<div class="login-left">
						<h3>INFORMATION</h3>
						<p>L'&eacute;space administrateur est reserv&eacute; uniquement &agrave; l'administration du site Spike Shoes, seul les personnes ayant l'autorisation d'y acceder peuvent voir le contenu. </p>
						
					</div>
					<div class="login-right">
						<h3>SE CONNECTER EN TANT QU'ADMINISTRATEUR</h3>
						<p>Entrez votre adresse mail suivi de votre mots de passe</p>
						<form method="POST" action="fonction/login.php">
							<div>
								<span>Addresse email<label>*</label></span>
								<input type="text" name="email"> 
							</div>
							<div>
								<span>Mots de passe<label>*</label></span>
								<input type="password" name="passe"> 
							</div>
							<input type="submit" value="Connexion" />
						</form>
					</div>
				</div>
			</div>
		</div>
		<?php include('footer.php'); ?>
	</body>
</html>


<!----start-image-slider---->
		<div class="img-slider">
			<div class="wrap">
			<ul id="jquery-demo">
			  <li>
			    <img src="web/images/slide-1.jpg" alt="Nike MERCURIAL VAPOR FG Coloris" />
			    <div class="slider-detils">
			    	<h3>CHAUSSURE DE FOOT <label>Confortable</label></h3>
			    	<span>Verrouillage lateral. Confort maximise. Traction assur&eacute;e.</span>
			    	
			    </div>
			  </li>
			   <li>
				    <img src="web/images/slide-4.jpg"  alt="Nike FLYWIRE MG Vert" title="Nike FLYWIRE MG Vert" />
				    <div class="slider-detils">
				    	<h3>CHAUSSURE DE FOOT <label>Amorti Leg&egrave;r</label></h3>
				    	<span>Controle de balle parfait. Toucher precis. Amorti leger.</span>
				    </div>
			    </li>
			  <li>
			    <img src="web/images/slide-2.jpg" alt="Nike MERCURIAL VAPOR FG Rouge" title="Nike MERCURIAL VAPOR FG Rouge" />
			    <div class="slider-detils">
			    	<h3>CHAUSSURE DE FOOT <label>Plus Souple</label></h3>
			    	<span> Traction ultra rapide assuree. Maintien sur. Grande souplesse.</span>
			    </div>
			  </li>
			</ul>
			</div>
		</div>
		<div class="clear"> </div>
		
		<div class="wrap">
			<div class="price-rage">
				<h3>S&eacute;lection hebdomadaire:</h3>
				<div id="slider-range">
				</div>
			</div>
		</div>

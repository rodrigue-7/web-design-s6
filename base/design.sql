CREATE TABLE IF NOT EXISTS `autres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` blob NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prix` int(11) DEFAULT NULL,
  `categorie` varchar(30) DEFAULT NULL,
  `title` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;


CREATE TABLE IF NOT EXISTS `client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) DEFAULT NULL,
  `prenom` varchar(30) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `pass` varchar(30) DEFAULT NULL,
  `ref` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;


INSERT INTO `client` (`id`, `nom`, `prenom`, `email`, `pass`, `ref`) VALUES
(1, 'ANDRIANOMENJANAHARY', 'Rodrigue', 'rodrigue@gmail.com', 'root', 1);

CREATE TABLE IF NOT EXISTS `image_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idProduit` int(11) DEFAULT NULL,
  `image` blob NOT NULL,
  `nom` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idProduit` (`idProduit`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `produit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` blob NOT NULL,
  `nom` varchar(100) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `prix` int(11) DEFAULT NULL,
  `reference` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

